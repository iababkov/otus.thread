﻿using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Xml;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private string _file;

        public string File { set { _file = value; } }

        public List<Customer> Parse()
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(_file);

            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Customer");

            var custList = new List<Customer>();

            foreach (XmlNode node in nodeList)
            {
                var enumerator = node.GetEnumerator();

                Customer cust = new Customer();

                while (enumerator.MoveNext())
                {
                    XmlElement el = (XmlElement) enumerator.Current;
                    switch (el.Name)
                    {
                        case "Id":
                            if (int.TryParse(el.InnerText, out int num))
                            {
                                cust.Id = num;
                            }
                            break;
                        case "FullName":
                            cust.FullName = el.InnerText;
                            break;
                        case "Email":
                            cust.Email = el.InnerText;
                            break;
                        case "Phone":
                            cust.Phone = el.InnerText;
                            break;
                    }
                }

                custList.Add(cust);
            }

            return custList;
        }
    }
}