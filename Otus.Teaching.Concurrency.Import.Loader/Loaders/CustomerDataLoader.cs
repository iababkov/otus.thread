﻿using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class CustomerDataLoader : IDataLoader
    {
        private readonly List<Customer>         _custList;
        private readonly byte                   _threadCount;
        private readonly bool                   _useThreadPool;
        private static readonly CountdownEvent  _loadFinishedEvent = new CountdownEvent(1);

        public CustomerDataLoader(List<Customer> custList, byte threadCount, bool useThreadPool = false)
        {
            _custList = custList;
            _threadCount = (byte)(threadCount > 0 ? threadCount : 1);
            _useThreadPool = useThreadPool;
        }

        public void LoadData()
        {
            if (_custList != null &&
                _custList.Count > 0)
            {
                int threadCapacity = (_custList.Count / _threadCount) + (_custList.Count % _threadCount > 0 ? 1 : 0);
                var threadCustList = new List<Customer>(threadCapacity);
                int custCountDown = _custList.Count;

                foreach (Customer cust in _custList)
                {
                    threadCustList.Add(cust);

                    custCountDown--;

                    if (threadCustList.Count == threadCapacity ||
                        custCountDown == 0)
                    {
                        _loadFinishedEvent.AddCount();
                        int с = _loadFinishedEvent.CurrentCount;

                        if (_useThreadPool)
                        {
                            ThreadPool.QueueUserWorkItem(SaveData, threadCustList);
                        }
                        else
                        {
                            Thread myThread = new Thread(new ParameterizedThreadStart(SaveData));
                            myThread.Start(threadCustList);
                        }

                        if (custCountDown != 0)
                        {
                            threadCustList = new List<Customer>(threadCapacity);
                        }
                    }
                }

                _loadFinishedEvent.Signal();
                _loadFinishedEvent.Wait();
            }
        }

        public static void SaveData(object list)
        {
            if (list is List<Customer> custList)
            {
                foreach (Customer cust in custList)
                {
                    Thread.Sleep(10); // Fake database insert query
                }
            }

            _loadFinishedEvent.Signal();
        }
    }
}
