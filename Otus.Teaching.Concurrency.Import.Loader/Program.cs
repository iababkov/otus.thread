﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static readonly string  _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private const string            SkipRunInProcessArg = "skipRunInProcess";
        private const int               NumberOfCustomers = 1000;
        private const byte              ThreadCount = 20;
        private const bool              UseThreadPool = false;

        static void Main(string[] args)
        {
            bool runInProcess = true;

            if (args != null &&
                args.Length > 0)
            {
                switch (args[0])
                {
                    case SkipRunInProcessArg:
                        runInProcess = false;
                        break;
                }
            }

            if (runInProcess)
            {
                var procInfo = new ProcessStartInfo
                {
                    FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Assembly.GetExecutingAssembly().FullName.Split(',')[0] + ".exe"),
                    Arguments = SkipRunInProcessArg
                };

                Process.Start(procInfo);
            }
            else
            {
                Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

                GenerateCustomersDataFile();

                var parser = new XmlParser
                {
                    File = _dataFilePath
                };

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                var loader = new CustomerDataLoader(parser.Parse(), ThreadCount, UseThreadPool);
                loader.LoadData();

                stopWatch.Stop();
                Console.WriteLine($"Load time: {stopWatch.Elapsed}");
            }
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, NumberOfCustomers);
            xmlGenerator.Generate();
        }
    }
}